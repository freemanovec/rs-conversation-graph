pipeline {
  agent {
    docker {
      image 'ekidd/rust-musl-builder'
    }
  }

  options {
    gitLabConnection('gitlab.com')
  }

  stages {
    stage('Init') {
      parallel {
        stage('Notify GitLab') {
          steps {
            updateGitlabCommitStatus name: 'build', state: 'running'
          }
        }

        stage('Set toolchain') {
          steps {
            sh 'rustup default stable-gnu'
          }
        }
      }
    }

    stage('Initial build') {
      parallel {
        stage('Debug') {
          steps {
            sh 'cargo build'
          }
        }

        stage('Release') {
          steps {
            sh 'cargo build --release'
          }
        }

        stage('Test') {
          steps {
            sh 'cargo test'
          }
        }

      }
    }

    stage('Preprocess') {
      parallel {
        stage('Fix lint') {
          steps {
            sh 'cargo fix --allow-dirty'
          }
        }

        stage('Formatting') {
          steps {
            sh 'cargo fmt'
          }
        }

      }
    }

    stage('Finalization') {
      parallel {
        stage('Debug') {
          steps {
            sh 'cargo build'
          }
        }

        stage('Release') {
          steps {
            sh 'cargo build --release'
          }
        }

        stage('Documentation') {
          steps {
            sh 'cargo doc'
          }
        }

      }
    }

  }

  post {
      success {
          updateGitlabCommitStatus name: 'build', state: 'success'
      }
      failure {
          updateGitlabCommitStatus name: 'build', state: 'failed'
      }
      aborted {
          updateGitlabCommitStatus name: 'build', state: 'canceled'
      }
  }
}