use super::{node::BuiltNode, Graph, graph_event::GraphEvent, node_switching::NodeSwitching};
use std::rc::Rc;

pub struct GraphPosition<TInput, TState> where TInput: PartialEq, TState: PartialEq {
    pub state: TState,
    graph: Rc<Graph<TInput, TState>>,
    current_node: Rc<BuiltNode<TInput, TState>>,
    last_served: ServedElement,
    switch_target_node: Option<Rc<BuiltNode<TInput, TState>>>,
    no_next_nodes_possible: bool
}

impl<TInput, TState> GraphPosition<TInput, TState> where TInput: PartialEq, TState: PartialEq {

    pub fn new(graph: Rc<Graph<TInput, TState>>, start_node_name: &str, state: TState) -> Result<Self, String> {
        let start_node = graph.nodes.iter().find(|node| &node.inner.name[..] == start_node_name);
        if start_node.is_none() {
            return Err("Unable to find the start node".to_string());
        }
        let start_node = start_node.unwrap().clone();
        let gp: GraphPosition<TInput, TState> = GraphPosition {
            state,
            graph,
            current_node: start_node,
            last_served: ServedElement::None,
            switch_target_node: None,
            no_next_nodes_possible: false
        };

        Ok(gp)
    }

    pub fn reached_end(&self) -> bool {
        self.no_next_nodes_possible
    }

    pub fn submit_input(&self, txt: TInput) {
        self.current_node.inner.input_buffer.replace(Some(txt));
    }

}

impl<TInput, TState> Iterator for GraphPosition<TInput, TState> where TInput: PartialEq, TState: PartialEq {

    type Item = GraphEvent;

    fn next(&mut self) -> Option<Self::Item> {

        if self.last_served == ServedElement::None {
            if self.current_node.inner.fun_pre.borrow().is_some() {
                //println!("Chain: '{}' - PRE", &self.current_node.inner.name);
                let fun = self.current_node.inner.fun_pre.replace(None).unwrap();
                fun(&mut self.state);
                self.current_node.inner.fun_pre.replace(Some(fun));
            }
            self.last_served = ServedElement::Input;
            if self.current_node.inner.requires_input {
                //println!("Chain: '{}' - INPUT", &self.current_node.inner.name);
                return Some(GraphEvent::Input);
            }
        }

        if self.last_served == ServedElement::Input {
            let buffer = if self.current_node.inner.requires_input {
                self.current_node.inner.input_buffer.replace(None)
            } else {
                None
            };

            if self.current_node.inner.fun_main.borrow().is_some() {
                //println!("Chain: '{}' - MAIN", &self.current_node.inner.name);
                let fun = self.current_node.inner.fun_main.replace(None).unwrap();
                fun(&mut self.state, buffer);
                self.current_node.inner.fun_main.replace(Some(fun));
                if self.current_node.next_nodes.len() == 1 {
                    let node = self.current_node.next_nodes[0].clone();
                    let node = self.graph.nodes.iter().find(|el| el.inner.name == node);
                    if node.is_none() {
                        panic!("Attempted autoswitch to unbuilt node");
                    }
                    let node = node.unwrap().clone();
                    //println!("Chain: '{0}' - SWITCH TO {0}", &self.current_node.inner.name);
                    self.switch_target_node = Some(node);
                }
            } else if self.current_node.inner.fun_main_switching.borrow().is_some() {
                //println!("Chain: '{}' - MAIN SWITCH", &self.current_node.inner.name);
                let fun = self.current_node.inner.fun_main_switching.replace(None).unwrap();
                let switching_target = fun(&mut self.state, buffer);
                self.current_node.inner.fun_main_switching.replace(Some(fun));
                match switching_target {
                    NodeSwitching::SwitchBackToSelf => {
                        //println!("Chain: '{0}' - SWITCH TO {0}", &self.current_node.inner.name);
                        self.switch_target_node = Some(self.current_node.clone());
                    },
                    NodeSwitching::SwitchWithEntry(next_node_name) => {
                        //println!("Chain: '{}' - SWITCH TO {}", &self.current_node.inner.name, &next_node_name);
                        let node = if self.current_node.inner.name == next_node_name {
                            Some(&self.current_node.inner.name)
                        } else {
                            self.current_node.next_nodes.iter().find(|el| el == &&next_node_name)
                        };
                        if node.is_none() {
                            panic!("Attempted switch to non-existing / unconnected node");
                        }
                        let node = node.unwrap();
                        let node = self.graph.nodes.iter().find(|el| &el.inner.name == node);
                        if node.is_none() {
                            panic!("Attempted switch to unbuilt node");
                        }
                        let node = node.unwrap().clone();
                        self.switch_target_node = Some(node);
                    },
                    NodeSwitching::CarryOn => {
                        match self.current_node.next_nodes.len() {
                            0 => panic!("No connection for successful carry-on switch"),
                            1 => {},
                            _ => panic!("Too many connections for deterministic carry-on")
                        }
                        let node = &self.current_node.next_nodes[0];
                        let node = self.graph.nodes.iter().find(|el| &el.inner.name == node);
                        if node.is_none() {
                            panic!("Attempted carry-on switch to unbuilt node");
                        }
                        let node = node.unwrap().clone();
                        self.switch_target_node = Some(node);
                    }
                }
            }
            
            let original_node = self.current_node.clone();
            let fun = if self.current_node.inner.fun_post.borrow().is_some() {
                //println!("Chain: '{}' - POST", &self.current_node.inner.name);
                Some(self.current_node.inner.fun_post.replace(None).unwrap())
            } else {
                None
            };

            let mut res: Option<Self::Item> = None;

            let mut execute_post = true;
            if self.switch_target_node.is_some() {
                // switch
                if self.current_node.inner.name == self.switch_target_node.as_ref().unwrap().inner.name {
                    // switching back to self, skip post
                    execute_post = false;
                }
                self.current_node = self.switch_target_node.as_ref().unwrap().clone();
                self.switch_target_node = None;
                self.last_served = ServedElement::None;
                res = Some(GraphEvent::Step);
            } else {
                // no next node, hit end
                self.no_next_nodes_possible = true;
            }

            if fun.is_some() {
                let fun = fun.unwrap();
                if execute_post {
                    fun(&mut self.state);
                }
                original_node.inner.fun_post.replace(Some(fun));
            }

            return res;

        }

        None

    }

}

#[derive(PartialEq)]
enum ServedElement {
    None,
    Input
}