#[derive(PartialEq)]
pub struct Connection {
    pub node_from: String,
    pub node_to: String
}

impl Connection {

    pub fn new(
        node_from: &str,
        node_to: &str
    ) -> Self {
        Self {
            node_from: String::from(node_from),
            node_to: String::from(node_to)
        }
    }

}
