use super::node_switching::NodeSwitching;

pub enum GraphFunction<'a> {
    Empty(&'a Box<dyn FnOnce() -> ()>),
    String(&'a Box<dyn FnOnce(String) -> ()>, String),
    SwitchingString(&'a Box<dyn FnOnce(String) -> NodeSwitching>, String)
}