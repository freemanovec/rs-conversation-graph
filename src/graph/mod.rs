use node::BuiltNode;
use std::rc::Rc;

pub mod node;
pub mod connection;
pub mod node_switching;
pub mod builder;
pub mod graph_position;
pub mod graph_event;
pub mod graph_function;

pub struct Graph<TInput, TState> where TState: PartialEq, TInput: PartialEq {
    nodes: Vec<Rc<BuiltNode<TInput, TState>>>
}

impl<TInput, TState> Graph<TInput, TState> where TState: PartialEq, TInput: PartialEq {

    pub fn new(
        nodes: Vec<BuiltNode<TInput, TState>>
    ) -> Graph<TInput, TState> {

        Graph {
            nodes: nodes.into_iter().map(|node| Rc::from(node)).collect::<Vec<Rc<BuiltNode<TInput, TState>>>>()
        }

    }


}