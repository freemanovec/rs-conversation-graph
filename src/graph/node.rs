use super::node_switching::NodeSwitching;
use std::cell::RefCell;

pub struct Node<TInput, TState> where TState: PartialEq, TInput: PartialEq {
    pub name: String,
    pub fun_pre: RefCell<Option<Box<dyn Fn(&mut TState) -> ()>>>,
    pub fun_main: RefCell<Option<Box<dyn Fn(&mut TState, Option<TInput>) -> ()>>>,
    pub fun_main_switching: RefCell<Option<Box<dyn Fn(&mut TState, Option<TInput>) -> NodeSwitching>>>,
    pub fun_post: RefCell<Option<Box<dyn Fn(&mut TState) -> ()>>>,
    pub requires_input: bool,
    accepted_values: Option<Vec<TInput>>,
    input_validation: RefCell<Option<Box<dyn Fn(&TInput) -> bool>>>,
    pub input_buffer: RefCell<Option<TInput>>
}

impl<TInput, TState> PartialEq for Node<TInput, TState> where TInput: PartialEq, TState: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        &self.name[..] == &other.name[..]
    }
}

impl<TInput, TState> Node<TInput, TState> where TInput: PartialEq, TState: PartialEq {

    pub fn new(name: &str) -> Self {
        Self {
            name: String::from(name),
            fun_pre: RefCell::new(None),
            fun_main: RefCell::new(None),
            fun_main_switching: RefCell::new(None),
            fun_post: RefCell::new(None),
            requires_input: false,
            accepted_values: None,
            input_validation: RefCell::new(None),
            input_buffer: RefCell::new(None)
        }
    }

    pub fn pre(self, fun: Box<dyn Fn(&mut TState) -> ()>) -> Self {
        self.fun_pre.replace(Some(fun));
        self
    }

    pub fn main(self, fun: Box<dyn Fn(&mut TState, Option<TInput>) -> ()>) -> Self {
        self.fun_main.replace(Some(fun));
        self.fun_main_switching.replace(None);
        self
    }

    pub fn main_switching(self, fun: Box<dyn Fn(&mut TState, Option<TInput>) -> NodeSwitching>) -> Self {
        self.fun_main_switching.replace(Some(fun));
        self.fun_main.replace(None);
        self
    }

    pub fn post(self, fun: Box<dyn Fn(&mut TState) -> ()>) -> Self {
        self.fun_post.replace(Some(fun));
        self
    }

    pub fn requires_input(mut self) -> Self {
        self.requires_input = true;
        self
    }

    pub fn input(self, input: TInput) -> Self {
        if self.requires_input {

            let mut is_good = true;

            if self.accepted_values.is_some() {
                let accepted_values = self.accepted_values.as_ref().unwrap();
                if !accepted_values.contains(&input) {
                    is_good = false;
                }
            }

            if is_good && self.input_validation.borrow().is_some() {
                let fun = self.input_validation.replace(None).unwrap();
                is_good = fun(&input);
                self.input_validation.replace(Some(fun));
            }
            
            if is_good {
                self.input_buffer.replace(Some(input));
            }

        }
        self
    }

    pub fn choices(mut self, choices: Vec<TInput>) -> Self {
        self.accepted_values = Some(choices);
        self
    }

    pub fn input_validator(self, validator: Box<dyn Fn(&TInput) -> bool>) -> Self {
        self.input_validation.replace(Some(validator));
        self
    }

}

pub struct BuiltNode<TInput, TState> where TInput: PartialEq, TState: PartialEq {
    pub inner: Node<TInput, TState>,
    pub next_nodes: Vec<String>
}

impl<TInput, TState> BuiltNode<TInput, TState> where TInput: PartialEq, TState: PartialEq {

    pub fn new(inner: Node<TInput, TState>, next_nodes: Vec<String>) -> Self {
        Self {
            inner,
            next_nodes
        }
    }

}