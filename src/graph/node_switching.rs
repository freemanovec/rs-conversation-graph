pub enum NodeSwitching {
    SwitchWithEntry(String),
    SwitchBackToSelf,
    CarryOn
}