use super::{node::{BuiltNode, Node}, connection::Connection, Graph};

pub struct GraphBuilder<TInput, TState> where TInput: PartialEq, TState: PartialEq {
    nodes: Vec<Node<TInput, TState>>,
    connections: Vec<Connection>
}

impl<'a, TInput, TState> GraphBuilder<TInput, TState> where TInput: PartialEq, TState: PartialEq {

    pub fn new() -> Self {
        Self {
            nodes: Vec::new(),
            connections: Vec::new()
        }
    }

    pub fn add_node(mut self, node: Node<TInput, TState>) -> Self {
        if !self.nodes.contains(&node) {
            self.nodes.push(node);
        }
        self
    }

    pub fn add_connection(mut self, connection: Connection) -> Self {
        if !self.connections.contains(&connection) {
            self.connections.push(connection);
        }
        self
    }

    pub fn connect(self, from: &Node<TInput, TState>, to: &Node<TInput, TState>) -> Self {
        let connection = Connection::new(&from.name, &to.name);
        self.add_connection(connection)
    }

    pub fn build(self) -> Result<Graph<TInput, TState>, String> {

        let built_nodes = {
            let connections = self.connections;
            let nodes = self.nodes;
            let built_nodes = nodes.into_iter().map(|node| {
                let connected_nodes: Vec<String> = connections.iter().filter_map(|connection| {
                    if connection.node_from == &node.name[..] {
                        Some(connection.node_to.clone())
                    } else {
                        None
                    }
                }).collect::<Vec<String>>();
                let built_node = BuiltNode::new(node, connected_nodes);
                built_node
            }).collect::<Vec<BuiltNode<TInput, TState>>>();
            built_nodes
        };
        
        Ok(Graph::new(
            built_nodes
        ))

    }

}
