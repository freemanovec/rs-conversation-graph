use std::{cell::RefCell, rc::Rc};
use conversation_graph::graph::{node::Node, builder::GraphBuilder, node_switching::NodeSwitching, connection::Connection, graph_position::GraphPosition, graph_event::GraphEvent};

extern crate conversation_graph;

#[test]
fn run() {
    let favorite_animal: Rc<RefCell<Option<String>>> = Rc::new(RefCell::new(None));
    let favorite_color: Rc<RefCell<Option<String>>> = Rc::new(RefCell::new(None));
    let output: Rc<RefCell<Option<String>>> = Rc::new(RefCell::new(None));

    let graph_builder = GraphBuilder::new();

    let node_start = Node::new("start")
        .main(Box::new(|_:_, _: _| {
            println!("Welcome to Fursona Picker!");
        }));
    let node_favorite_animal_favorite_animal = favorite_animal.clone();
    let node_favorite_animal = Node::new("favorite_animal")
        .requires_input()
        .pre(Box::new(|_: _| {
            println!("Tell me, what is your favorite animal?");
        }))
        .main(Box::new(move |_: _, inp: Option<String>| {
            if let Some(inp) = inp {
                println!("{}? A wise choice.", &inp);
                node_favorite_animal_favorite_animal.replace(Some(inp));
            }
        }));
    let node_favorite_color_favorite_color = favorite_color.clone();
    let node_favorite_color = Node::new("favorite_color")
        .requires_input()
        .pre(Box::new(|_: _| {
            println!("Now tell me your favorite color.");
        }))
        .main(Box::new(move |_: _, inp: Option<String>| {
            if let Some(inp) = inp {
                node_favorite_color_favorite_color.replace(Some(inp));
            }
        }));
    let node_consent = Node::<String, ()>::new("consent")
        .requires_input()
        .choices(vec![
            "Yes".to_string(),
            "No".to_string()
            ])
        .pre(Box::new(|_: _| {
            println!("Do you want to know your sona? (Yes/No)");
        }))
        .main_switching(Box::new(|_: _, inp: Option<String>| {
            if let Some(inp) = inp {
                let next_node_name = match &inp[..] {
                    "Yes" => "display_sona",
                    "No" => "favorite_animal",
                    _ => "consent"
                };
                NodeSwitching::SwitchWithEntry(String::from(next_node_name))
            } else {
                NodeSwitching::SwitchBackToSelf
            }
        }));
    let node_display_sona_output = output.clone();
    let node_display_sona = Node::new("display_sona")
        .main(Box::new(move |_: _, _: _| {
            let out = format!("Alright! An ideal sona for you would be a {} {}", &favorite_color.borrow().as_ref().unwrap(), &favorite_animal.borrow().as_ref().unwrap());
            node_display_sona_output.replace(Some(out.clone()));
            println!("{}", out);
        }));
    let node_end = Node::new("end")
        .main(Box::new(|_: _, _: _| {
            println!("Thanks for using this tool >.<");
        }));

    let con_start_favorite_animal = Connection::new("start", "favorite_animal");
    let con_favorite_animal_favorite_color = Connection::new("favorite_animal", "favorite_color");
    let con_favorite_color_consent = Connection::new("favorite_color", "consent");
    let con_consent_favorite_animal = Connection::new("consent", "favorite_animal");
    let con_consent_display_sona = Connection::new("consent", "display_sona");
    let con_display_sona_end = Connection::new("display_sona", "end");

    let graph = graph_builder
        .add_node(node_start)
        .add_node(node_favorite_animal)
        .add_node(node_favorite_color)
        .add_node(node_consent)
        .add_node(node_display_sona)
        .add_node(node_end)
        .add_connection(con_start_favorite_animal)
        .add_connection(con_favorite_animal_favorite_color)
        .add_connection(con_favorite_color_consent)
        .add_connection(con_consent_favorite_animal)
        .add_connection(con_consent_display_sona)
        .add_connection(con_display_sona_end)
        .build().expect("Unable to build graph");
    let graph = Rc::from(graph);

    let mut prepared_inputs = vec![
        "husky",
        "light blue",
        "No",
        "husky",
        "dark blue",
        "OwO",
        "Yes"
    ].into_iter().map(|el| String::from(el)).rev().collect::<Vec<String>>();
    let mut graph_position = GraphPosition::new(graph.clone(), "start", ()).expect("Unable to construct graph position pointer");
    while !graph_position.reached_end() {
        while let Some(graph_blocking_event) = graph_position.next() {
            match graph_blocking_event {
                GraphEvent::Input => {
                    //println!("Chain: READ");
                    let read: String = prepared_inputs.pop().expect("No item in prepared input");
                    println!("-> {}", &read);
                    graph_position.submit_input(read);
                },
                GraphEvent::Step => {
                    //println!("Chain: STEP");
                }
            }
        }
    }

    let output = output.borrow();
    let output = output.as_ref().unwrap();
    assert_eq!(&output[..], "Alright! An ideal sona for you would be a dark blue husky");
}