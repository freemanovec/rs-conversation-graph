use std::{cell::RefCell, rc::Rc};
use conversation_graph::graph::{node::Node, builder::GraphBuilder, node_switching::NodeSwitching, graph_position::GraphPosition, graph_event::GraphEvent};

extern crate conversation_graph;

#[test]
fn run() {
    let output: Rc<RefCell<Option<String>>> = Rc::new(RefCell::new(None));

    let graph_builder = GraphBuilder::new();

    let node_start = Node::new("start")
        .main_switching(Box::new(|state: &mut RefCell<u8>, _: _| {
            let val = *(state.borrow());
            if val < 10 {
                state.replace(val + 1);
                NodeSwitching::SwitchBackToSelf
            } else {
                NodeSwitching::CarryOn
            }
        }));
    let output_borrowed = output.clone();
    let node_end =  Node::<(), RefCell<u8>>::new("end")
        .main(Box::new(move |state: _, _: _| {
            let out = Some(format!("Final counter value: {}", state.borrow()));
            output_borrowed.replace(out);
        }));

    let graph = graph_builder
        .connect(&node_start, &node_end)
        .add_node(node_start)
        .add_node(node_end)
        .build().expect("Unable to build graph");
    let graph = Rc::from(graph);

    let mut graph_position = GraphPosition::new(graph.clone(), "start", RefCell::new(0)).expect("Unable to construct graph position pointer");
    while !graph_position.reached_end() {
        while let Some(graph_blocking_event) = graph_position.next() {
            match graph_blocking_event {
                GraphEvent::Input => {
                    panic!();
                },
                GraphEvent::Step => {
                }
            }
        }
    }

    let output = output.borrow();
    let output = output.as_ref().unwrap();
    assert_eq!(&output[..], "Final counter value: 10");
}